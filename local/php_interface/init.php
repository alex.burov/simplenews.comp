<?php

use Bitrix\Main\Application;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Loader;
Loader::includeModule( "iblock" );

// инфоблок новостей
define( 'IBLOCK_NEWS', 1 );


// вывод данных
function pr( $var, $type = false )
{
	echo '<pre style="font-size:10px; border:1px solid #000; background:#FFF; text-align:left; color:#000;">';
	if ( $type ) {
		var_dump( $var );
	} else {
		print_r( $var );
	}
	echo '</pre>';
}

